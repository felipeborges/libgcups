/* gcups-client.h
 *
 * Copyright (C) 2017 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GCUPS_CLIENT_H
#define GCUPS_CLIENT_H

#include <glib-object.h>
#include <gio/gio.h>

#include "gcups-printer.h"

G_BEGIN_DECLS

#define GCUPS_TYPE_CLIENT (gcups_client_get_type())

G_DECLARE_DERIVABLE_TYPE (GcupsClient, gcups_client, GCUPS, CLIENT, GObject)

#define GCUPS_CLIENT_UNSET_PORT -1

struct _GcupsClientClass
{
  GObjectClass parent;
};

GcupsClient *gcups_client_new (const gchar *hostname);

void         gcups_client_get_printers (GcupsClient          *client,
                                        GCancellable         *cancellable,
                                        GAsyncReadyCallback   callback,
                                        gpointer              user_data);

/**
 * gcups_client_get_printers_finish:
 * @client: a #GcupsClient
 *
 * Obtains the list of printers in the default cups server.
 *
 * Returns: (element-type GcupsPrinter) (transfer container): A #GList
 * containing the printers.
 **/
GList       *gcups_client_get_printers_finish (GcupsClient   *client,
                                               GAsyncResult  *result,
                                               GError       **error);

G_END_DECLS

#endif /* GCUPS_CLIENT_H */
