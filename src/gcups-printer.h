/* gcups-printer.h
 *
 * Copyright (C) 2017 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GCUPS_PRINTER_H
#define GCUPS_PRINTER_H

#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define GCUPS_TYPE_PRINTER (gcups_printer_get_type())

G_DECLARE_DERIVABLE_TYPE (GcupsPrinter, gcups_printer, GCUPS, PRINTER, GObject)

#define GCUPS_PRINTER_UNSET_PORT -1

struct _GcupsPrinterClass
{
  GObjectClass parent;
};

GcupsPrinter *gcups_printer_new (const gchar *name);

gchar        *gcups_printer_get_name (GcupsPrinter *printer);

G_END_DECLS

#endif /* GCUPS_PRINTER_H */
