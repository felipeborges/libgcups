/* gcups-printer.c
 *
 * Copyright (C) 2017 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cups/cups.h>

#include "gcups-printer.h"

typedef struct
{
  gchar *name;
} GcupsPrinterPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GcupsPrinter, gcups_printer, G_TYPE_OBJECT)

#define GCUPS_PRINTER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), GCUPS_TYPE_PRINTER, GcupsPrinterPrivate))

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

GcupsPrinter *
gcups_printer_new (const gchar *name)
{
  return g_object_new (GCUPS_TYPE_PRINTER,
                       "name", name,
                       NULL);
}

static void
gcups_printer_finalize (GObject *object)
{
  GcupsPrinter *self = (GcupsPrinter *)object;
  GcupsPrinterPrivate *priv = gcups_printer_get_instance_private (self);

  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (gcups_printer_parent_class)->finalize (object);
}

static void
gcups_printer_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  GcupsPrinter *self = GCUPS_PRINTER (object);
  GcupsPrinterPrivate *priv = GCUPS_PRINTER_PRIVATE (self);

  switch (prop_id)
    {
      case PROP_NAME:
        g_value_set_string (value, priv->name);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gcups_printer_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  GcupsPrinter *self = GCUPS_PRINTER (object);
  GcupsPrinterPrivate *priv = GCUPS_PRINTER_PRIVATE (self);

  switch (prop_id)
    {
      case PROP_NAME:
        g_clear_pointer (&priv->name, g_free);
        priv->name = g_value_dup_string (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gcups_printer_class_init (GcupsPrinterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gcups_printer_finalize;
  object_class->get_property = gcups_printer_get_property;
  object_class->set_property = gcups_printer_set_property;

  g_object_class_install_property (object_class, PROP_NAME,
    g_param_spec_string ("name",
                         "Name",
                         "The Name",
                         NULL,
                         G_PARAM_READWRITE));
}

static void
gcups_printer_init (GcupsPrinter *printer)
{
}

gchar *
gcups_printer_get_name (GcupsPrinter *printer)
{
    GcupsPrinterPrivate *priv = GCUPS_PRINTER_PRIVATE (printer);

    return priv->name;
}
