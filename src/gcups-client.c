/* gcups-client.c
 *
 * Copyright (C) 2017 Felipe Borges <felipeborges@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cups/cups.h>

#include "gcups-client.h"

/**
 * SECTION:gcupsclient
 * @Short_description: The main entrypoint for accessing the printing API
 * @Title: GcupsClient
 *
 * The GcupsClient object is the main entrypoint for managing a printing
 * server.
 */

typedef struct
{
  gchar *hostname;
  gint   port;
} GcupsClientPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GcupsClient, gcups_client, G_TYPE_OBJECT)

#define GCUPS_CLIENT_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), GCUPS_TYPE_CLIENT, GcupsClientPrivate))

enum {
  PROP_0,
  PROP_HOSTNAME,
  PROP_PORT,
  N_PROPS
};

void   gcups_client_get_printers (GcupsClient            *client,
                                  GCancellable           *cancellable,
                                  GAsyncReadyCallback     callback,
                                  gpointer                user_data);

GList *gcups_client_get_printers_finish (GcupsClient     *client,
                                         GAsyncResult    *result,
                                         GError         **error);

GcupsClient *
gcups_client_new (const gchar *hostname)
{
  return g_object_new (GCUPS_TYPE_CLIENT,
                       "hostname", hostname,
                       NULL);
}

static void
gcups_client_finalize (GObject *object)
{
  GcupsClient *self = (GcupsClient *)object;
  GcupsClientPrivate *priv = gcups_client_get_instance_private (self);

  g_clear_pointer (&priv->hostname, g_free);

  G_OBJECT_CLASS (gcups_client_parent_class)->finalize (object);
}

static void
gcups_client_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  GcupsClient *self = GCUPS_CLIENT (object);
  GcupsClientPrivate *priv = GCUPS_CLIENT_PRIVATE (self);

  switch (prop_id)
    {
      case PROP_HOSTNAME:
        g_value_set_string (value, priv->hostname);
        break;
      case PROP_PORT:
        g_value_set_int (value, priv->port);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gcups_client_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  GcupsClient *self = GCUPS_CLIENT (object);
  GcupsClientPrivate *priv = GCUPS_CLIENT_PRIVATE (self);

  switch (prop_id)
    {
      case PROP_HOSTNAME:
        g_clear_pointer (&priv->hostname, g_free);
        priv->hostname = g_value_dup_string (value);
        break;
      case PROP_PORT:
        priv->port = g_value_get_int (value);
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gcups_client_class_init (GcupsClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gcups_client_finalize;
  object_class->get_property = gcups_client_get_property;
  object_class->set_property = gcups_client_set_property;

  g_object_class_install_property (object_class, PROP_HOSTNAME,
    g_param_spec_string ("hostname",
                         "Hostname",
                         "The hostname",
                         NULL,
                         G_PARAM_READWRITE));

  g_object_class_install_property (object_class, PROP_PORT,
    g_param_spec_int ("port",
                      "Port",
                      "The port",
                      -1, G_MAXINT32, GCUPS_CLIENT_UNSET_PORT,
                      G_PARAM_READWRITE));
}

static void
gcups_client_init (GcupsClient *self)
{
  GcupsClientPrivate *priv = GCUPS_CLIENT_PRIVATE (self);

  priv->port = GCUPS_CLIENT_UNSET_PORT;
}

static void
_gcups_client_get_printers_thread (GTask        *task,
                                   gpointer      object,
                                   gpointer      task_data,
                                   GCancellable *cancellable)
{
  GList *printers = NULL;
  cups_dest_t *dests;
  gint num_dests, i;

  num_dests = cupsGetDests (&dests);

  for (i = 0; i < num_dests; i++) {
    GcupsPrinter *printer = gcups_printer_new (dests[i].name);
    printers = g_list_append (printers, printer);
  }

  if (g_task_set_return_on_cancel (task, FALSE)) {
    g_task_return_pointer (task, printers, (GDestroyNotify) g_list_free);
  }
}

void
gcups_client_get_printers (GcupsClient *client,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback,
                           gpointer            user_data)
{
  GTask *task;

  task = g_task_new (client, cancellable, callback, user_data);
  g_task_set_return_on_cancel (task, TRUE);
  g_task_run_in_thread (task, (GTaskThreadFunc) _gcups_client_get_printers_thread);
  g_object_unref (task);
}

GList *
gcups_client_get_printers_finish (GcupsClient   *client,
                                  GAsyncResult  *result,
                                  GError       **error)
{
  g_return_val_if_fail (g_task_is_valid (result, client), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}
