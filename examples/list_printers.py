from gi.repository import Gcups, GLib

client = Gcups.Client();

def on_list_printers (source, result):
    printers = []
    try:
        printers = source.get_printers_finish (result);
    except Exception as e:
        print("ERROR" + e.message)

    for printer in printers:
        print (printer.get_name ())

client.get_printers (None, on_list_printers)

mainloop = GLib.MainLoop()
mainloop.run()
